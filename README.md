# ⚠ repo MOVED on [CripCompareView](https://gitlab.com/CripCompareView/crypcompareview)

---

<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">crypto-compare-view</h3>

This project was generated using [Nx](https://nx.dev).

---

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

This is a simple nx Mono repo with an Angular as Frontend and Nest as Backend. It shows the price of some crypto currencies.

## 🏁 Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

### Prerequisites

You need to install these packages:

- [Nx](https://nx.dev)
- [Yarn](https://yarnpkg.com/) or [Npm](https://nodejs.org/en/)
- [Angular](https://angular.io/)
- [Nestjs](https://nestjs.com/)

### Installing

For install all deps you need to perform this commad:

```sh
yarn
```

or

```sh
npm install
```

## 🔧 [WIP] Running the tests <a name = "tests"></a>

This porject use Jest and Cypress as testing tools. At moment all tests are in draft and they may not work.

## 🎈 Usage <a name="usage"></a>

You can run this project with these command (may take a while, please, be patient):

```sh
yarn start:all
```

or

```sh
npm run start:all
```

After the start up, you can find the frontend app at http://localhost:4200/ and the Swagger doc of the backend at http://localhost:3000/api.

## 🚀 [WIP] Deployment <a name = "deployment"></a>

This project has been designed to be deployed on a [Docker Swarm](https://docs.docker.com/engine/swarm/swarm-tutorial/) cluster and the provisioning could be done with an [Ansible](https://www.ansible.com/) playbook.
The CI/CD is in draft yet but the idea is to use gitlab ci to run test, container scanning, building and deploy on cluster swarm.

## ✍️ Authors <a name = "authors"></a>

- [@donny88](https://github.com/DonNy88) - Idea & Initial work
