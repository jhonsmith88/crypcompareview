ARG APP=api
ARG BUILD_CONFIGURATION=production
ARG PORT=3000
ARG NODE_TAG=14.16.1-alpine

FROM node:${NODE_TAG} AS base
ARG PORT
EXPOSE ${PORT}
RUN apk add --no-cache tini
ENTRYPOINT ["tini", "--"]

FROM node:${NODE_TAG} AS dependecies
WORKDIR /app
COPY package.json yarn.lock* ./
RUN yarn install:ci --production

FROM dependecies AS build
ARG APP
ARG BUILD_CONFIGURATION
RUN yarn install:ci
COPY . .
RUN yarn build ${APP} --configuration=${BUILD_CONFIGURATION} --verbose

FROM base
ARG APP
RUN mkdir ./app && chown -R node:node ./app
WORKDIR /app
# For security propuse avoid root user
USER node
COPY --from=dependecies --chown=node:node /app/node_modules/ ./node_modules
COPY --from=build --chown=node:node /app/dist/apps/${APP} .
COPY --from=build --chown=node:node /app/apps/${APP}/healthcheck.js .
HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
  CMD node ./healthcheck.js
CMD ["node", "main.js"]
