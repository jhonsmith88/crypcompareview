import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';
import { CryptoCurrencies } from '@crypto-compare-view/api-interfaces';
import { interval, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';

@Component({
  selector: 'crypto-compare-view-home',
  template: `
    <nz-layout class="container">
      <!-- TODO: create a Sider separate component -->
      <nz-sider
        nzCollapsible
        [(nzCollapsed)]="isCollapsed"
        [nzWidth]="width"
        [nzReverseArrow]="isReverseArrow"
      >
        <ul
          nz-menu
          [nzTheme]="'dark'"
          [nzMode]="'inline'"
          [nzInlineCollapsed]="isCollapsed"
        >
          <!-- TODO: Use *ngFor -->
          <li nz-menu-item (click)="onSelectCurrency('USD')">
            <span>
              <i nz-icon nzType="dollar-circle" nzTheme="outline"></i>
              <span class="nav-text">US Dollar</span>
            </span>
          </li>
          <li nz-menu-item (click)="onSelectCurrency('GBP')">
            <span>
              <i nz-icon nzType="pound-circle" nzTheme="outline"></i>
              <span class="nav-text">Pound Sterling</span>
            </span>
          </li>
          <li nz-menu-item (click)="onSelectCurrency('EUR')">
            <span>
              <i nz-icon nzType="euro-circle" nzTheme="outline"></i>
              <span class="nav-text">Euro</span>
            </span>
          </li>
          <li nz-menu-item (click)="onSelectCurrency('JPY')">
            <span>
              <i nz-icon nzType="pay-circle" nzTheme="outline"></i>
              <span class="nav-text">Japanese Yen</span>
            </span>
          </li>
          <li nz-menu-item (click)="onSelectCurrency('ZAR')">
            <span>
              <i nz-icon nzType="trademark-circle" nzTheme="outline"></i>
              <span class="nav-text">South African Rand</span>
            </span>
          </li>
        </ul>
      </nz-sider>
      <nz-layout>
        <nz-header class="header">
          <h2 nz-typography>Crypto Compare View</h2>
        </nz-header>
        <nz-content class="content">
          <span style="padding:6px"></span>
          <div class="table-view">
            <crypto-compare-view-table
              [listOfData$]="currencies$"
            ></crypto-compare-view-table>
          </div>
        </nz-content>
        <nz-footer class="footer-text">
          <i nz-icon nzType="copyright" nzTheme="outline"></i>
          Copyright BGB Group
        </nz-footer>
      </nz-layout>
    </nz-layout>
  `,
  styles: [
    `
      .container {
        height: 100%;
      }

      .header {
        background: #fff;
        padding: 0;
        text-align: center;
      }

      .content {
        margin: 0 16px;
      }

      .table-view {
        padding: 0px;
        background: #fff;
        height: 95%;
      }

      .footer-text {
        text-align: center;
      }
    `,
  ],
})
export class HomeComponent implements OnInit {
  isCollapsed = false;
  isReverseArrow = false;
  width = 200;
  currency = 'USD';

  currencies$: Observable<CryptoCurrencies[]>;

  constructor(private readonly homeService: HomeService) {}

  ngOnInit(): void {
    this.currencies$ = interval(5000).pipe(
      startWith(0),
      switchMap(() => this.homeService.getCryptoPrice(this.currency))
    );
  }

  onSelectCurrency(currency: string) {
    this.currency = currency;
  }
}
