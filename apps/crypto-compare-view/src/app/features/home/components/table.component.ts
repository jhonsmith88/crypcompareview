import { Component, Input } from '@angular/core';

import { CryptoCurrencies } from '@crypto-compare-view/api-interfaces';
import { Observable } from 'rxjs';

@Component({
  selector: 'crypto-compare-view-table',
  template: `
    <!-- TODO: Add filter and sorting feat - https://ng.ant.design/components/table/en -->
    <nz-table [nzData]="listOfData$ | async">
      <thead>
        <tr>
          <th>Currancy</th>
          <th>Price</th>
        </tr>
      </thead>
      <!-- TODO: Usw ngFor - https://ng.ant.design/components/table/en -->
      <!-- TODO: Display a loading spinner while the first http request is action to avoid all errors on app startup -->
      <tbody>
        <tr>
          <td>{{ (listOfData$ | async)[0]?.name }}</td>
          <td>{{ (listOfData$ | async)[0]?.price }}</td>
        </tr>
        <tr>
          <td>{{ (listOfData$ | async)[1]?.name }}</td>
          <td>{{ (listOfData$ | async)[1]?.price }}</td>
        </tr>
        <tr>
          <td>{{ (listOfData$ | async)[2]?.name }}</td>
          <td>{{ (listOfData$ | async)[2]?.price }}</td>
        </tr>
        <tr>
          <td>{{ (listOfData$ | async)[3]?.name }}</td>
          <td>{{ (listOfData$ | async)[3]?.price }}</td>
        </tr>
        <tr>
          <td>{{ (listOfData$ | async)[4]?.name }}</td>
          <td>{{ (listOfData$ | async)[4]?.price }}</td>
        </tr>
        <tr>
          <td>{{ (listOfData$ | async)[5]?.name }}</td>
          <td>{{ (listOfData$ | async)[5]?.price }}</td>
        </tr>
      </tbody>
    </nz-table>
  `,
})
export class TableComponent {
  @Input() listOfData$: Observable<CryptoCurrencies[]>;
}
