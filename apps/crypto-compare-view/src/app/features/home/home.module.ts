import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { NzUiKitModule } from '../../core/nz-ui-kit/nz-ui-kit.module';
import { HomeRoutingModule } from './home-routing.module';
import { TableComponent } from './components/table.component';
import { HttpClientModule } from '@angular/common/http';
import { HomeService } from './home.service';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [HomeComponent, TableComponent],
  imports: [CommonModule, HomeRoutingModule, NzUiKitModule],
  providers: [HomeService],
})
export class HomeModule {}
