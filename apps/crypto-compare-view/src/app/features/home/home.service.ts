import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { interval } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ApiResponse } from '@crypto-compare-view/api-interfaces';
import { environment } from '../../../environments/environment';

@Injectable()
export class HomeService {
  constructor(private readonly http: HttpClient) {}

  getCryptoPrice(currency: string) {
    return this.http
      .get<ApiResponse>(`${environment.api}/${currency}`)
      .pipe(map((res) => res.currencies));
  }
}
