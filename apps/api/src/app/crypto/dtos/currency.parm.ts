import { ApiProperty } from '@nestjs/swagger';

export class CryptoCurrenciesParam {
  @ApiProperty({
    example: 'USD',
    enum: ['USD', 'GBP', 'EUR', 'JPY', 'ZAR'],
    type: String,
  })
  currency: string;
}
