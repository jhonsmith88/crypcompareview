import { Module, Global } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';

import * as Joi from 'joi';

import { ApiConfigService } from './api-config.service';

const validationSchema = Joi.object({
  NODE_ENV: Joi.string()
    .valid('local', 'development', 'stage', 'production')
    .default('local'),
  PORT: Joi.number().default(3000),
  CRYPTO_COMPARE_API_KEY: Joi.string().alphanum().required(),
  CRYPTO_COMPARE_API_URL: Joi.string().uri().required(),
});

@Module({
  exports: [ApiConfigService],
  imports: [ConfigModule.forRoot({ validationSchema })],
  providers: [ApiConfigService, ConfigService],
})
@Global()
export class ApiConfigModule {}
