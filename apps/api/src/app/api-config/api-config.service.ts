import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ApiConfigService {
  private logger: Logger;

  constructor(private readonly configService: ConfigService) {
    this.logger = new Logger(ApiConfigService.name);
    this.log();
  }

  get isProduction(): boolean {
    return this.nodeEnvironment === 'production';
  }

  get isLocalDevelopment(): boolean {
    return this.nodeEnvironment === 'local';
  }

  get isStage(): boolean {
    return this.nodeEnvironment === 'stage';
  }

  get isDevelopment(): boolean {
    return this.nodeEnvironment === 'development';
  }

  private get nodeEnvironment(): string {
    return this.configService.get<string>('NODE_ENV');
  }

  get port(): number {
    return this.configService.get<number>('PORT');
  }

  get apiKey(): string {
    return this.configService.get<string>('CRYPTO_COMPARE_API_KEY');
  }

  get apiUrl(): string {
    return this.configService.get<string>('CRYPTO_COMPARE_API_URL');
  }

  private log() {
    const { logger } = this;
    logger.log(`Running in ${this.nodeEnvironment} mode`);
    logger.log(`Listening on port ${this.port}`);
  }
}
