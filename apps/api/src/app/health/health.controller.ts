import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { HealthCheck, HealthCheckResult } from '@nestjs/terminus';

@Controller('health')
@ApiTags('Health')
export class HealthController {
  @Get('readiness')
  @HealthCheck()
  readiness(): Promise<HealthCheckResult> {
    // TODO: add a properly healh check
    return Promise.resolve({ status: 'ok' } as HealthCheckResult);
  }

  @Get('liveness')
  liveness(): Promise<HealthCheckResult> {
    // TODO: add a properly healh check
    return Promise.resolve({ status: 'ok' } as HealthCheckResult);
  }
}
